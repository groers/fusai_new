from bp_env.env_def import UnitType, RED_AIRPORT_ID, BLUE_AIRPORT_ID, MapInfo, SideType, TeamStatus
from common.interface.base_rule import BaseRulePlayer
from bp_env.env_cmd import EnvCmd, Point
from player.unit_ob import UnitOb
from player.attack_list import AttackList
from player.formation import Formation, FormationStatus
from player.red.parameters import *
from copy import deepcopy

class AgentState:
    DEPLOY_STATE = 10  # 部署单位，起飞单位
    FORMATION_STATE = 20  # 将飞机编入编队
    DECISION_STATE = 30  # 决定攻打哪个岛屿
    SOUTH_ATTACK_STATE = 40  # 攻打南岛阶段
    SOUTH_ATTACK_STATE_PARSE1 = 41  # 攻打南岛已经达到南部集合点
    SOUTH_ATTACK_STATE_PARSE2 = 42  # 攻打南岛已经达到南部突击点

    NORTH_ATTACK_STATE = 50  # 攻打北岛阶段
    WAITING_FOR_REINFORCEMENT = 60  # 等待支援


class RulePlayer(BaseRulePlayer):
    SOUTH_A2A_TEAM_NUM = 3
    SOUTH_A2G_TEAM_NUM = 5
    NORTH_A2A_TEAM_NUM = 3
    NORTH_A2G_TEAM_NUM = 4
    AWACS_ESCORT_TEAM_NUM = 2

    name_cmds_map = {}  # key为编队名，value为编队包含的单位所执行过的命令

    def __init__(self, side):
        super().__init__(side)
        self.en_side = 'blue' if side == 'red' else 'red'
        self.agent_state = AgentState.DEPLOY_STATE

        self.unit_ob = UnitOb(side)
        self.attack_list = AttackList(self.side)
        self.formation_map = {}  # key为编队名，value为编队
        self.firstly_attack_target = ""  # 首先打击的目标岛屿，'south'或者'north'

        self.awacs_id = 0
        self.disturb_id = 0
        self.ship_ids = [0, 0]
        self.south_command_id = 0
        self.north_command_id = 0

        self.task_formations = []  # 执行攻岛任务的编队

    # 打印出当前已部署单位各自的数目
    def _get_units(self, raw_obs):
        unit_map = {}
        for unit in raw_obs[self.side]['units']:
            unit_map[unit['LX']] = unit_map.get(unit['LX'], 0) + 1
        print(unit_map)

    # 打印出已发现的敌方单位各自的数目
    def _get_en_units(self, raw_obs):
        unit_map = {}
        for unit in raw_obs[self.side]['qb']:
            unit_map[unit['LX']] = unit_map.get(unit['LX'], 0) + 1
        print(unit_map)

    def _a2g_attack(self, raw_obs, en_id):
        cmds = []
        for unit in raw_obs[self.side]['units']:
            if unit['LX'] == UnitType.A2G and unit['WP']['360'] > 0:  # and unit['WP']['360'] > 0
                cmds.append(EnvCmd.make_targethunt(unit['ID'], en_id, 270, 100))
                # break
        return cmds

    def step_update(self, raw_obs):
        cmds = []
        self.unit_ob.update(raw_obs)
        cmds_, return_ids = self.attack_list.make_update(self.unit_ob)  # 新增一个返回值，返回返航集合
        cmds.extend(cmds_)

        rm_formations = []
        for name, formation in self.formation_map.items():
            formation.update(self.attack_list, self.unit_ob, return_ids)
            if formation.is_empty():
                rm_formations.append(name)

        # # 从编队字典中删除空编队。暂时这么设置，万一以后要留着往其中补充单位呢
        # for name in rm_formations:
        #     del self.formation_map[name]

        if self.disturb_id not in self.unit_ob.ids[self.side][UnitType.DISTURB]:
            self.disturb_id = 0
        if self.awacs_id not in self.unit_ob.ids[self.side][UnitType.AWACS]:
            self.awacs_id = 0
        if self.south_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.south_command_id = 0
        if self.north_command_id not in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
            self.north_command_id = 0
        return cmds

    # 将单位分进编队里
    def add_unit2formation(self, raw_obs):
        for name, cmds in self.name_cmds_map.items():
            if name not in self.formation_map:
                self.formation_map[name] = Formation(self.side, name, TeamStatus.AREA_PATROL, self.unit_ob)
            str_cmd = [str(cmd).replace("\'", "\"") for cmd in cmds]  # team['Task']其实是个字符串，要比较的话，需要对cmd做str
            for team in raw_obs[self.side]['teams']:
                if team['Task'] in str_cmd:
                    for unit_tuple in team['PT']:
                        self.formation_map[name].add_unit(team['LX'], unit_tuple[0])

    def formation_patrol_attack(self, grand_target_types=None, air_target_types=None):
        if grand_target_types is None:
            grand_target_types = Formation.GRAND_ATTACK_TYPES
        if air_target_types is None:
            air_target_types = Formation.RED_A2A_ATTACK_TYPES
        cmds = []
        for name, formation in self.formation_map.items():
            cmd = formation.make_patrol_attack(self.attack_list, grand_target_types, air_target_types)
            print(name, "attack_cmd", cmd)
            cmds.extend(cmd)
        return cmds

    def step(self, raw_obs):
        cmds = []
        cmds.extend(self.step_update(raw_obs))
        print("state:", self.agent_state)
        if self.agent_state == AgentState.DEPLOY_STATE:
            # 设置队名和命令的映射表
            self.name_cmds_map['a2a_fn'] = []
            self.name_cmds_map['a2a_fc'] = []
            self.name_cmds_map['a2a_fs'] = []
            self.name_cmds_map['a2a_bn'] = []
            self.name_cmds_map['a2a_bs'] = []
            self.name_cmds_map['awacs'] = []
            self.name_cmds_map['a2a_escort_1'] = []
            self.name_cmds_map['a2a_escort_2'] = []
            self.name_cmds_map['a2a_escort_3'] = []
            self.name_cmds_map['hunt'] = []

            map_ = formation_name_reside_point_map
            # 起飞巡航歼击机
            for formation_name in ['a2a_fn', 'a2a_fs', 'a2a_fc', 'a2a_bn', 'a2a_bs']:
                point = map_[formation_name]
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *point, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map[formation_name].append(cmd)
                cmds.append(cmd)

            # 设置预警机
            for unit in raw_obs[self.side]['units']:
                if unit['LX'] == UnitType.AWACS:
                    self.awacs_id = unit['ID']
                    break
            cmd = EnvCmd.make_awcs_areapatrol(self.awacs_id, *AWACS_PATROL_POINT, *AWACS_AREA_PATROL_PARAMS)
            self.name_cmds_map['awacs'].append(cmd)
            cmds.append(cmd)

            # 起飞突击护航歼击机
            for formation_name in ['a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                point = map_[formation_name]
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2A_TEAM_SIZE, UnitType.A2A, *point, *A2A_AREA_PATROL_PARAMS)
                self.name_cmds_map[formation_name].append(cmd)
                cmds.append(cmd)

            for i in range(9):
                cmd = EnvCmd.make_takeoff_areapatrol(
                    RED_AIRPORT_ID, A2G_TEAM_SIZE, UnitType.A2G, *A2G_ASSEMBLY_POINT, *AREA_PATROL_PARAMS)
                self.name_cmds_map['hunt'].append(cmd)
                cmds.append(cmd)  # 起飞轰炸机

            cmd = EnvCmd.make_takeoff_areapatrol(
                RED_AIRPORT_ID, 1, UnitType.DISTURB, *A2G_ASSEMBLY_POINT, *AREA_PATROL_PARAMS)
            self.name_cmds_map['hunt'].append(cmd)
            cmds.append(cmd)  # 起飞干扰机

            # 设置舰船
            self.ship_ids = deepcopy(self.unit_ob.ids[self.side][UnitType.SHIP])
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[0], *SHIP1_DEPLOY_POINT, 270, 1))
            cmds.append(EnvCmd.make_ship_movedeploy(self.ship_ids[1], *SHIP2_DEPLOY_POINT, 270, 1))
            cmds.append(EnvCmd.make_ship_areapatrol(self.ship_ids[0], *AWACS_PATROL_POINT, *SHIP_AREA_PATROL_PARAMS))

            # 获取地方指挥所信息
            for command_id in self.unit_ob.ids[self.en_side][UnitType.COMMAND]:
                command_unit = self.unit_ob.id_map[self.en_side][command_id]
                if command_unit.y > 0:
                    self.north_command_id = command_id
                else:
                    self.south_command_id = command_id
            self.agent_state = AgentState.FORMATION_STATE
            return cmds  # 这里要返回，以便于调用下一次step更新raw_obs

        # 将飞机编入对应的编队
        if self.agent_state == AgentState.FORMATION_STATE:
            self.add_unit2formation(raw_obs)  # 将飞机编入编队中
            self.name_cmds_map.clear()
            self.disturb_id = self.unit_ob.ids[self.side][UnitType.DISTURB][0]
            for name, point in formation_name_reside_point_map.items():
                self.formation_map[name].set_reside_point(*point)
            for _, formation in self.formation_map.items():
                formation.print_formation()
            cmds.extend(self.formation_patrol_attack())
            self.agent_state = AgentState.DECISION_STATE
            return cmds

        # 开始决定攻岛
        if self.agent_state == AgentState.DECISION_STATE:
            awacs_unit = self.unit_ob.id_map[self.side][self.awacs_id]
            print("dis", awacs_unit.compute_2d_distance(AWACS_PATROL_POINT[0], AWACS_PATROL_POINT[1]))
            cmds.extend(self.formation_patrol_attack())
            # 计算预警机是否已到达既定点
            hunt_fm_avai = self.formation_map['hunt'].is_units_available()
            if awacs_unit.compute_2d_distance(AWACS_PATROL_POINT[0], AWACS_PATROL_POINT[1]) > 10000 or not hunt_fm_avai:
                return cmds
            north_force = 0
            south_force = 0
            for en_id in self.unit_ob.ids[self.en_side][UnitType.A2A]:
                en_unit = self.unit_ob.id_map[self.en_side][en_id]
                if en_unit.y >= 0:
                    north_force += 1
                else:
                    south_force += 1
            for en_id in self.unit_ob.ids[self.en_side][UnitType.SHIP]:
                en_unit = self.unit_ob.id_map[self.en_side][en_id]
                if en_unit.y >= 0:
                    north_force += 4
                else:
                    south_force += 4
            self.firstly_attack_target = 'south' if south_force <= north_force + 2 else 'north'  # 南岛好打一些，补充一点指数
            print("决定了我们就打：", self.firstly_attack_target)
            cmds.extend(self.formation_patrol_attack())
            self.firstly_attack_target = 'south'  # 需要修改
            if self.firstly_attack_target == 'south':
                self.agent_state = AgentState.SOUTH_ATTACK_STATE
                for formation_name, point in formation_name_reside_point_map_hunt_south.items():
                    self.formation_map[formation_name].set_reside_point(*point)
                    self.task_formations.append(self.formation_map[formation_name])
            else:
                self.agent_state = AgentState.NORTH_ATTACK_STATE
            for formation in self.task_formations:
                formation.st = FormationStatus.READY
            return cmds

        # 攻打南岛步骤：
        if self.agent_state // 10 == AgentState.SOUTH_ATTACK_STATE // 10:
            for name, formation in self.formation_map.items():
                if name not in ['hunt', 'a2a_escort_1', 'a2a_escort_2', 'a2a_escort_3']:
                    cmds.extend(formation.make_patrol_attack(self.attack_list))

            hunt_fm = self.formation_map.get('hunt')
            escort_fm_1 = self.formation_map.get('a2a_escort_1')
            escort_fm_2 = self.formation_map.get('a2a_escort_2')
            escort_fm_3 = self.formation_map.get('a2a_escort_3')
            id_map = self.unit_ob.id_map[self.side]

            # 需要修改
            if hunt_fm.is_empty():
                cmds.extend(self.formation_patrol_attack())
                self.agent_state = AgentState.WAITING_FOR_REINFORCEMENT
                return cmds
            # 这里还需要修改
            if self.south_command_id == 0:
                retreat_line = [Point(*[-216929, -254155, 8000]), Point(*SOUTH_A2G_ASSEMBLY_POINT),
                                Point(*A2G_ASSEMBLY_POINT)]
                for formation in self.task_formations:
                    for id_ in formation.ids():
                        cmds.append(EnvCmd.make_linepatrol(id_, 800, 1, 'line', retreat_line))
                self.agent_state = 67  # 需要修改
                return cmds

            if self.disturb_id is not 0:
                # 选取距离指挥所最近的一个轰炸机的地点作为干扰机巡航目标点
                aid_id = hunt_fm.pt_ids[UnitType.A2G][0]
                min_dis = id_map[aid_id].compute_2d_distance(BLUE_SOUTH_COMMANDER_POINT[0],
                                                             BLUE_SOUTH_COMMANDER_POINT[1])
                aid_point = [id_map[aid_id].x, id_map[aid_id].y, 8000]
                for id_ in hunt_fm.pt_ids[UnitType.A2G]:
                    unit = id_map[id_]
                    dis = unit.compute_2d_distance(BLUE_SOUTH_COMMANDER_POINT[0], BLUE_SOUTH_COMMANDER_POINT[1])
                    if dis < min_dis:
                        aid_point = [unit.x, unit.y, 8000]
                        min_dis = dis
                        aid_id = id_
                cmds.append(EnvCmd.make_areapatrol(self.disturb_id, *aid_point, *AREA_PATROL_PARAMS))  # 干扰机紧跟轰炸机

            # 判断当前子阶段
            # 在集合点需要全部飞机到期，在突击点只需要轰炸机到齐
            # 需要修改
            if hunt_fm.has_all_arrived(*hunt_fm.reside_point) \
                    and self.agent_state == AgentState.SOUTH_ATTACK_STATE:
                    # and (escort_fm_1.is_empty() or escort_fm_1.has_all_arrived(*escort_fm_1.reside_point)) \
                    # and (escort_fm_1.is_empty() or escort_fm_1.has_all_arrived(*escort_fm_1.reside_point)) \
                    # and (escort_fm_1.is_empty() or escort_fm_1.has_all_arrived(*escort_fm_1.reside_point)) \
                    # and self.agent_state == AgentState.SOUTH_ATTACK_STATE:
                self.agent_state = AgentState.SOUTH_ATTACK_STATE_PARSE1
                hunt_fm.set_reside_point(*SOUTH_A2G_HUNT_POINT)
                hunt_fm.st = FormationStatus.READY  # 设置ready状态，以便于发起新的move指令
                escort_fm_1.set_reside_point(*SOUTH_A2A_L_HUNT_POINT)
                escort_fm_1.st = FormationStatus.READY
                escort_fm_2.set_reside_point(*SOUTH_A2A_C_HUNT_POINT)
                escort_fm_2.st = FormationStatus.READY
                escort_fm_3.set_reside_point(*SOUTH_A2A_R_HUNT_POINT)
                escort_fm_3.st = FormationStatus.READY

            # 需要修改
            if hunt_fm.has_all_arrived(
                    *hunt_fm.reside_point) and self.agent_state == AgentState.SOUTH_ATTACK_STATE_PARSE1:
                self.agent_state = AgentState.SOUTH_ATTACK_STATE_PARSE2
                hunt_fm.set_reside_point(*BLUE_SOUTH_COMMANDER_POINT)
                hunt_fm.st = FormationStatus.READY

            for formation in self.task_formations:
                if formation.name == 'hunt':
                    cmds.extend(
                        formation.make_line_hunt(UnitType.A2G, self.attack_list, [UnitType.SHIP, UnitType.COMMAND]))
                else:
                    cmds.extend(formation.make_line_hunt(UnitType.A2A, self.attack_list))
            return cmds

        else:
            print("来到了else")
            print(cmds)

        return cmds
