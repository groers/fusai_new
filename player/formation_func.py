# 巡逻打击，轰炸机，歼击机自动打击周围攻击范围内的敌方单位
    def make_patrol_attack(self, attack_list, group_target_list=None, air_target_list=None):
        if group_target_list is None:
            group_target_list = []  # 对地打击优先级列表
        if air_target_list is None:
            air_target_list = []  # 对空打击优先级列表

        cmds = []
        a2a_attack_list = []  # 空中被打击目标列表
        a2g_attack_list = []  # 在地被打击目标列表
        for type_ in group_target_list:
            for en_unit_id in self.unit_ob.ids[self.en_side][type_]:
                if en_unit_id not in attack_list.lists or \
                        len(attack_list.lists[en_unit_id]) < self.NEED_ATTACK_NUM[type_]:
                    a2g_attack_list.append(en_unit_id)
        for type_ in air_target_list if self.side == 'red' else self.BLUE_A2A_ATTACK_LIST:
            for en_unit_id in self.unit_ob.ids[self.en_side][type_]:
                if en_unit_id not in attack_list.lists or \
                        len(attack_list.lists[en_unit_id]) < self.NEED_ATTACK_NUM[type_]:
                    a2a_attack_list.append(en_unit_id)

        for en_id in a2a_attack_list:
            rm_list = []
            ready_list = []  # 在攻击距离内准备好的单位的id
            en_unit = self.unit_ob.id_map[self.en_side][en_id]
            en_type = en_unit.unit_type
            for a2a_id in self.available_unit_ids[UnitType.A2A]:
                unit = self.unit_ob.id_map[self.side][a2a_id]
                dis = unit.compute_2d_distance_unit(en_unit)
                if dis < A2A_DISTANCE:
                    ready_list.append(a2a_id)
            need_num = self.NEED_ATTACK_NUM[en_type] if en_id not in attack_list.lists else \
                self.NEED_ATTACK_NUM[en_type] - len(attack_list.lists[en_id])
            if need_num > len(ready_list):  # 如果就绪的单位的数量不足于打击目标，那就少派一些单位
                need_num = len(ready_list)
            for i in range(need_num):
                id_ = ready_list[i]
                cmds.append(EnvCmd.make_airattack(id_, en_id, 1))
                rm_list.append(id_)
                self.available_unit_num[UnitType.A2A] -= 1
                attack_list.add_attack(id_, en_id)
            for id_ in rm_list:
                self.available_unit_ids[UnitType.A2A].remove(id_)

        for en_id in a2g_attack_list:
            rm_list = []
            ready_list = []  # 在攻击距离内准备好的单位的id
            en_unit = self.unit_ob.id_map[self.en_side][en_id]
            en_type = en_unit.unit_type
            for a2g_id in self.available_unit_ids[UnitType.A2G]:
                unit = self.unit_ob.id_map[self.side][a2g_id]
                dis = unit.compute_2d_distance_unit(en_unit)
                if dis < A2G_DISTANCE:
                    ready_list.append(a2g_id)
            need_num = self.NEED_ATTACK_NUM[en_type] if en_id not in attack_list.lists else \
                self.NEED_ATTACK_NUM[en_type] - len(attack_list.lists[en_id])
            if need_num > len(ready_list):  # 如果就绪的单位的数量不足于打击目标，那就少派一些单位
                need_num = len(ready_list)
            for i in range(need_num):
                id_ = ready_list[i]
                unit = self.unit_ob.id_map[self.side][id_]
                cmds.append(EnvCmd.make_targethunt(id_, en_id, unit.compute_2d_attack_angle_unit(en_unit), 100))
                rm_list.append(id_)
                self.available_unit_num[UnitType.A2G] -= 1
                attack_list.add_attack(id_, en_id)
            for id_ in rm_list:
                self.available_unit_ids[UnitType.A2G].remove(id_)
        return cmds