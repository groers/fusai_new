import math


# 计算两点间平面距离
def compute_2d_distance_point(x1, y1, x2, y2):
    d_x = x1 - x2
    d_y = y1 - y2
    return math.sqrt(d_x ** 2 + d_y ** 2)


# 计算点2相对于点1的方位角
def compute_2d_attack_angle_point(x1, y1, x2, y2):
    angle = 0.0
    dx = x2 - x1
    dy = y2 - y1
    if x2 == x1:
        angle = math.pi / 2.0
        if y2 == y1:
            angle = 0.0
        elif y2 < y1:
            angle = 3.0 * math.pi / 2.0
    elif x2 > x1 and y2 > y1:
        angle = math.atan(dx / dy)
    elif x2 > x1 and y2 < y1:
        angle = math.pi / 2 + math.atan(-dy / dx)
    elif x2 < x1 and y2 < y1:
        angle = math.pi + math.atan(dx / dy)
    elif x2 < x1 and y2 > y1:
        angle = 3.0 * math.pi / 2.0 + math.atan(dy / -dx)
    return int(angle * 180 / math.pi)


# （x1,y1）为被打击目标，假设敌方不动情况下，计算我方目标（x2, y2）打击地方目标时开始发弹的点,hunt_id为我方开火距离
def compute_2d_battle_point(x1, y1, x2, y2, hunt_dis):
    angle = compute_2d_attack_angle_point(x1, x2, y1, y2)  # 方位角
    x3 = x1 + math.sin(angle / 180 * math.pi) * hunt_dis
    y3 = y1 + math.cos(angle / 180 * math.pi) * hunt_dis
    return round(x3), round(y3)
