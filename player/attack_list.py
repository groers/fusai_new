from bp_env.env_def import UnitType, RED_AIRPORT_ID, BLUE_AIRPORT_ID
from bp_env.env_cmd import EnvCmd


# 打击列表类，负责管理正在执行的作战任务
class AttackList:
    def __init__(self, side):
        self.lists = {}  # 字典key为被打击目标id，value为所有打击平台组成的id列表
        self.task_ids = set()  # 所有正在执行打击任务的我方平台id
        self.side = side
        self.en_side = 'blue' if side == 'red' else 'red'

    # 根据新态势，更新打击列表，可能会调用返航命令，所以需要返回cmds
    def make_update(self, unit_ob):
        cmds = []
        rm_target_list = []
        return_ids = set()  # 返航的单位
        for target_id, list_ in self.lists.items():
            en_unit = unit_ob.id_map[self.en_side].get(target_id)
            rm_list = []
            for id_ in list_:
                unit = unit_ob.id_map[self.side].get(id_)
                if unit is not None:
                    missile_num = 0
                    for _, v in unit.info_map['WP'].items():
                        missile_num = v
                        break
                    if missile_num == 0:  # 导弹量为0则返航
                        rm_list.append(id_)
                        if unit.unit_type in [UnitType.A2A, UnitType.A2G]:
                            cmds.append(
                                EnvCmd.make_returntobase(id_, RED_AIRPORT_ID if self.side == 'red' else BLUE_AIRPORT_ID)
                            )
                            return_ids.add(id_)
                else:
                    rm_list.append(id_)
            if en_unit is not None:
                for id_ in rm_list:
                    list_.remove(id_)
                if len(list_) == 0:
                    rm_target_list.append(target_id)
            else:
                rm_target_list.append(target_id)
        for en_id in rm_target_list:
            del self.lists[en_id]
        self.task_ids = set()
        for _, list_ in self.lists.items():
            for id_ in list_:
                self.task_ids.add(id_)
        return cmds, return_ids

    # 增加打击任务。每次发起打击相关的指令时，都需要调用这个指令做一个记录
    def add_attack(self, id_, en_id):
        if en_id not in self.lists:
            self.lists[en_id] = []
        if id_ not in self.lists[en_id]:
            self.lists[en_id].append(id_)
            self.task_ids.add(id_)
            return True
        else:
            return False

    # 停止一个单位的所有打击任务
    def remove_unit(self, id_):
        rm_list = []
        for en_id, list_ in self.lists.items():
            if id_ in list_:
                self.lists[en_id].remove(id_)
            if len(self.lists[en_id]) == 0:
                rm_list.append(en_id)
        self.task_ids.discard(id_)
        for en_id in rm_list:
            del self.lists[en_id]
