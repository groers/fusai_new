from bp_env.env_def import UnitType, SideType
from player.units import Unit


# 汇集态势信息中的敌我双方信息
class UnitOb:
    def __init__(self, side):
        self.side = side
        self.en_side = 'blue' if side == 'red' else 'red'
        self.id_map = {SideType.RED: {}, SideType.BLUE: {}}  # 通过单位id获得单位信息的字典，内部字典key为id，value为Unit类型的单位
        self.ids = {  # 根据类型查询红蓝双方某种所有单位的id
            SideType.RED: {
                UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: [], UnitType.SHIP: [],
                UnitType.S2A: [], UnitType.RADAR: [], UnitType.COMMAND: [], UnitType.AIRPORT: [], UnitType.UN_AIR: [],
                UnitType.UN_SHIP: [],
            },
            SideType.BLUE: {
                UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: [], UnitType.SHIP: [],
                UnitType.S2A: [], UnitType.RADAR: [], UnitType.COMMAND: [], UnitType.AIRPORT: [], UnitType.UN_AIR: [],
                UnitType.UN_SHIP: [],
            },
        }

    # 根据态势更新类中属性信息
    def update(self, raw_obs):
        self.id_map = {SideType.RED: {}, SideType.BLUE: {}}
        self.ids = {
            SideType.RED: {
                UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: [], UnitType.SHIP: [],
                UnitType.S2A: [], UnitType.RADAR: [], UnitType.COMMAND: [], UnitType.AIRPORT: [], UnitType.UN_AIR: [],
                UnitType.UN_SHIP: [],
            },
            SideType.BLUE: {
                UnitType.A2A: [], UnitType.A2G: [], UnitType.AWACS: [], UnitType.DISTURB: [], UnitType.SHIP: [],
                UnitType.S2A: [], UnitType.RADAR: [], UnitType.COMMAND: [], UnitType.AIRPORT: [], UnitType.UN_AIR: [],
                UnitType.UN_SHIP: [],
            },
        }
        for unit in raw_obs[self.side]['units']:
            self.id_map[self.side][unit['ID']] = Unit(unit)
            self.ids[self.side][unit['LX']].append(unit['ID'])
        for qb in raw_obs[self.side]['qb']:
            self.id_map[self.en_side][qb['ID']] = Unit(qb)
            self.ids[self.en_side][qb['LX']].append(qb['ID'])
